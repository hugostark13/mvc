<?php

namespace Controllers;

class Master_Controller{
    
    protected $layout;
    protected $views_dir;
    public function __construct($views_dir = '/views/codi/' ) {
       //echo "Master of pupets";
       $this->views_dir = $views_dir;
       $this->layout = DX_ROOT_DIR.'/views/layouts/default.php';
    }
    
    public function index(){
        $page_title = 'Home';
        $templete_name = DX_ROOT_DIR.$this->views_dir.'index.php';
        include_once $this->layout;
    }
    
}