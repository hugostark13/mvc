<?php

namespace Controllers;

class Codi_Controller extends Master_Controller{
    
    public function __construct() {
   //     echo 'Home page'.'</br>';
   parent::__construct('/views/codi/');
    }
    
    
    public function index() {
    //    echo 'Am a Index() of Home Page'.'</br>';
        $page_title = 'Home';
        $templete_name = DX_ROOT_DIR.$this->views_dir.'index.php';
        include_once $this->layout;
    }

     public function about() {
     //   echo 'Am a Index() of Home Page'.'</br>';
        $page_title = 'About';
        $templete_name = DX_ROOT_DIR.$this->views_dir.'about.php';
        include_once $this->layout;
    }
    
     public function blog() {
    //    echo 'Am a Index() of Home Page'.'</br>';
        $page_title = 'Blog';
        $templete_name = DX_ROOT_DIR.$this->views_dir.'blog.php';
        include_once $this->layout;
    }
    
     public function contacts() {
     //   echo 'Am a Index() of Home Page'.'</br>';
        $page_title = 'Contacts';
        $templete_name = DX_ROOT_DIR.$this->views_dir.'contacts.php';
        include_once $this->layout;
    }
}
