
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js eq-ie9 ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- <base href="<?php echo $basehref; ?>"> -->
    <title><?php echo $page_title; ?></title> 
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="/mvc/css/normalize.css">
    <link rel="stylesheet" href="/mvc/css/styles.css">
    <?php if($page_title == 'Contacts') { ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXSvNhrGGECmJMZKs3Ewbz32jVRvLTTq4&language=en"></script>
    <?php } ?>
    <script type="text/javascript">
        var currentPage = '<?php echo $page_title; ?>';
    </script>
</head>
<body>
    <div class="wrapper clearfix" role="main">

        <header class="header clearfix">
           <div class="header-top clearfix">
               <div class="logo"><img src="/mvc/img/logo.png"></div>

               <div class="search clearfix">
                   <button class="search-btn"></button>
                   <input type="text">
               </div>
            </div>
           <nav class="nav">

                <ul class="navul">
                    <li class="navli"><a href="/mvc/codi/index" class="navlink" href="">HOME</a></li>
                    <li class="navli"><a href="/mvc/codi/about" class="navlink" href="">ABOUT</a></li>
                    <li class="navli"><a href="/mvc/codi/blog" class="navlink" href="">BLOG</a></li>
                    <li class="navli"><a href="/mvc/codi/contacts" class="navlink" href="">CONTACT</a></li>
                </ul>
           </nav>
        </header>