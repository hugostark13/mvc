         
        <footer class=" footer clearfix">
        <div class="social-wrapper clearfix">
            <div class="social-icons"><a href="#"><img src="/mvc/img/btn_twitter.png" alt=""></a></div>
            <div class="social-icons"><a href="#"><img src="/mvc/img/btn_rss.png" alt=""></a></div>
            <div class="social-icons"><a href="#"><img src="/mvc/img/btn_face.png" alt=""></a></div>
            <div class="social-icons"><a href="#"><img src="/mvc/img/btn_pin.png" alt=""></a></div>
        </div>
        <div class="copyright-wrapper">
            
            <h1 class="footer-text">Best Site Ever | hugoman@abv.bg</h1> 
            <h2 class="footer-subtext">CopyRight 2016 Grid Vision wwww.pranak-studio.com</h2>         
            
           </div>

        </footer>
    </div><!-- End of wrapper -->
    <script src="/mvc/js/libs/jquery-1.8.3.min.js"></script>
    <script>
        if (typeof jQuery == 'undefined') {
            var e = document.createElement('script');
            e.src = "/mvc/js/libs/jquery-1.8.3.min.js";
            e.type='text/javascript';
            document.getElementsByTagName("head")[0].appendChild(e);
        }
    </script>
    <script src="/mvc/js/application.js"></script>
</body>
</html>