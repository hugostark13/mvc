<section>
	<article class="title-module">
		<h1 class="title">Read Our Posts to Know Less.</h1>
		<h2 class="path">
			<a class="path-link" href="">all</a>/
			<a class="path-link" href="">news</a>/
			<a class="path-link" href="">design</a>/
			<a class="path-link" href="">print</a>/
			<a class="path-link" href="">art</a>/
		    <a class="path-link" href="">development</a>
		</h2>
	</article>

	<article class="box-module clearfix">
	<div class="box-wrapper clearfix">
	<div class="box-container-main">
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post1_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
			
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post5_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post9_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
	</div>

	<div class="box-container-main">
				<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post2_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
			
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post6_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post10_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
	</div>
	<div class="box-container-main">
				<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post3_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
			
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post7_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post11_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
	</div>
	<div class="box-container-main">
				<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post4_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
			
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post8_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
		<div class="box-container">
			<div class="box-header">
		   		<div class="box-header-imgwraper"><img src="/mvc/img/post12_img.png" alt=""></div>
				<div class="box-header-hover">
					<a href="#" class="box-header-link">
					<span class="hover-plus"></span>	
					<div class="hover-info clearfix">
						<div class="icons view">857</div>
						<div class="icons date">07/05/12</div>
						<div class="icons like">588</div>
					</div>
					</a>
				</div>
			</div>
			<div class="box-content">
				<h2 class="box-title">Sticker Mule</h2>
				<h3 class="box-info">June 15, 2012 / news,contests</h3>
				<p class="box-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi alias, dolorem fuga et.</p>
				<a href="" class="box-more">More</a>
			</div>
					
		</div>
		
	</div>
	</div>
	<div class="pagination">
		<a href="#" class="pagination-link">1</a>
		<a href="#" class="pagination-link">2</a>
		<a href="#" class="pagination-link">3</a>
		<a href="#" class="pagination-link">4</a>
			
	</div>
	
	</article>
</section>