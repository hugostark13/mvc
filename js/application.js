/**
 * Master application
 * Includes Classes Env, App
 */

window.App = window.App || {};
window.Env = window.Env || {};

(function($, App, Env){

    /**
     * Document ready
     */
    $(function() {

        switch (currentPage) {
            case 'Contacts':
            break;
        }

        /*-------------------------------------------------------
        Google Maps Initialization
        -------------------------------------------------------*/

        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng(42.149656, 24.744276),
                zoom: 17,
                panControl: false,
                zoomControl: true,
                scaleControl: true,
                scrollwheel: true,
                mapTypeControl: false,
                disableDoubleClickZoom: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var marker = new google.maps.Marker({
                         position: new google.maps.LatLng(42.149656, 24.744276),
                         map: map,
                         title: 'ул.Бетовен 6'
                         });
        }

        if(currentPage == 'Contacts') {
            initialize();
        }
    });

})($, App, Env);